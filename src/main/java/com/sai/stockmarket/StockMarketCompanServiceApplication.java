package com.sai.stockmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class StockMarketCompanServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockMarketCompanServiceApplication.class, args);
	}

}
