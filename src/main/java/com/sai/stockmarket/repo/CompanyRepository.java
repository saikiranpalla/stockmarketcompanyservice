package com.sai.stockmarket.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sai.stockmarket.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
	
	@Modifying
	@Query("UPDATE Company SET name=:comName WHERE id=:comId")
	int updateCompanyName(String comName,Long comId);
}
