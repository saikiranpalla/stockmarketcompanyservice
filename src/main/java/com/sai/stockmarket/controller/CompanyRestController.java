package com.sai.stockmarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sai.stockmarket.entity.Company;
import com.sai.stockmarket.exception.CompanyNotFoundException;
import com.sai.stockmarket.service.ICompanyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyRestController {
	
	@Autowired
	private ICompanyService service;
	
	//Create company
	@PostMapping("/create")
	public ResponseEntity<String> createCompany(@RequestBody Company company){
		log.info("ENTERED INTO SAVE METHOD");
		ResponseEntity<String> response = null;
		try {
			Long id = service.createCompany(company);
			response = ResponseEntity.ok("CREATED WITH ID : "+ id);
			log.info("COMPANY IS CREATED {}.",id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE SAVE METHOD");
		return response;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Company>> getAllCompanies(){
		log.info("ENTERED INTO FETCH ALL METHOD");
		ResponseEntity<List<Company>> response = null;
		try {
			List<Company> list = service.getAllCompanies();
			response = ResponseEntity.ok(list);
			log.info("FETCH ALL METHOD IS SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE FETCH ALL METHOD");
		return response;
	}
	
	//fetch one company
	@GetMapping("fetch/{id}")
	public ResponseEntity<Company> getOneCompany(@PathVariable Long id){
		log.info("ENTERED INTO FETCH ONE METHOD");
		ResponseEntity<Company> response = null;
		try {
			Company comp = service.getOneCompany(id);
			response = ResponseEntity.ok(comp);
			log.info("FETCH ONE METHOD IS SUCCESS");
		} catch (CompanyNotFoundException e) {
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE FETCH ONE METHOD");
		return response;
	}
	
	//delete one company
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteOneCompany(@PathVariable Long id){
		log.info("ENTERED INTO DELETE METHOD");
		ResponseEntity<String> response = null;
		try {
			service.deleteCompany(id);
			response = ResponseEntity.ok("Comapny Deleted");
			log.info("DELETE COMPANY IS SUCCESS");
		} catch (CompanyNotFoundException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE DELETE METHOD");
		return response;
	}
	
	//update Company
	@PutMapping("/modify")
	public ResponseEntity<String> updateComapany(@RequestBody Company company){
		log.info("ENTERED INTO UPDATE COMPANY");
		ResponseEntity<String> response = null; 
		try {
			service.updateCompany(company);
			response = ResponseEntity.ok("COMPANY UPDATED");
			log.info("UPDATE COMPANY IS SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE UPDATE METHOD");
		return response;
	}
	
	//update Company name

	@PatchMapping("/modiify/name/{id}/{name}")
	public ResponseEntity<String> updateCompanyName(
			@PathVariable Long id,
			@PathVariable String name
			)
	{
		ResponseEntity<String> response = null;
		log.info("ENTERED INTO PATCH METHOD");
		try {
			service.updateCompanyName(name, id);
			response = ResponseEntity.ok("COMPANY NAME UPDATED");
			log.info("COMPANY NAME UPDATED SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE COMPANY NAME");
		return response;
	}
}
