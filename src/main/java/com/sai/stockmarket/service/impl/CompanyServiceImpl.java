package com.sai.stockmarket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sai.stockmarket.entity.Company;
import com.sai.stockmarket.exception.CompanyNotFoundException;
import com.sai.stockmarket.repo.CompanyRepository;
import com.sai.stockmarket.service.ICompanyService;

@Service
public class CompanyServiceImpl implements ICompanyService {
	
	@Autowired
	private CompanyRepository repo;
	
	@Override
	public Long createCompany(Company com) {
		return repo.save(com).getId();
	}

	@Override
	public void updateCompany(Company com) {
		Long id = com.getId();
		if(id != null && repo.existsById(id))
		{
			repo.save(com);
		}else {
			throw new CompanyNotFoundException("Given '"+id+"' Not Exist");
		}
	}

	@Override
	public Company getOneCompany(Long id) {
		Optional<Company> com = repo.findById(id);
		if(com.isEmpty())
		{
			throw new CompanyNotFoundException("Given '"+id+"' Not Exist");
		}else {
			return com.get();
		}
	}

	@Override
	public List<Company> getAllCompanies() {
		return repo.findAll();
	}
	
	@Override
	public void deleteCompany(Long id) {
		repo.delete(getOneCompany(id));
	}
	
	@Override
	@Transactional
	public int updateCompanyName(String comName, Long id) {
		if(id!=null && repo.existsById(id)) {
			return repo.updateCompanyName(comName, id);
		}else {
			throw new CompanyNotFoundException("COMPANY '"+id+"' Not Exist");
		}
	}
}
