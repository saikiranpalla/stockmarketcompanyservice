package com.sai.stockmarket.service;

import java.util.List;

import com.sai.stockmarket.entity.Company;

public interface ICompanyService {
	
	Long createCompany(Company com);
	void updateCompany(Company com);
	Company getOneCompany(Long id);
	List<Company> getAllCompanies();
	void deleteCompany(Long id);
	int updateCompanyName(String comName,Long comId);

}
